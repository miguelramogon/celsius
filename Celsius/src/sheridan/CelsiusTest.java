package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class CelsiusTest {

	@Test
	public void testConverFromFahrenheitRegular() {
		assertTrue(Celsius.converFromFahrenheit(20) == -7 );
	}
	
	@Test
	public void testConverFromFahrenheitExcepional() {
		assertFalse(Celsius.converFromFahrenheit(25) == -7 );
	}
	
	@Test
	public void testConverFromFahrenheitBoundIn() {
		assertTrue(Celsius.converFromFahrenheit(6) == -14 );
	}
	
	@Test
	public void testConverFromFahrenheitBoundOut() {
		assertTrue(Celsius.converFromFahrenheit(7) == -14 );
	}

}
